---
name: Mecum
date: 2020-12-18
image: "./mecum.jpg"
link: "https://gitlab.com/CordlessWool/mecum"
---

Translated to English Mecum means "with me" and is the base for this profile page and open for everyone.

Feel free to clone and create your own page (everything without text and content is free to use). Text can easily edit via Markdown files.

Language and Framework: JavaScript, Gridsome, Vue, Markdown