---
name: Loom
date: 2016-01-02
image: "./loom.png"
link: "https://github.com/CordlessWool/loom"
---

The first own theme for my blog Storyloom is written for GhostJS and can be used as a template for blogs. It isn't updated anymore because I started a new Version based on Gridsome that use the GhostJS API (Loom 2.0)

Languages and tools: GhostJS, Handlebars, JQuery, CSS3, HTML5