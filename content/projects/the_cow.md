---
name: "Die Kuh bringt's"
date: 2012-04-01
image: "https://images.unsplash.com/photo-1546445317-29f4545e9d53?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&h=1000&q=80"
---
"Die Kuh bringt's" could be translated to the cow delivers. The base idea was an online shop for food directly from the farmer without another dealer between. The car should be covered in a cow look.

This was my first big project and get very close to going live, but my partner left the project shortly before and I had to do a mandatory internship and bachelor thesis.

Besides learning how to structure a big coding project, I learned everything to found a company, also including writing a market research and business plan

Languages and tools: VanillaJS, PHP, MySQL, HTML, CSS3

The code stays private.