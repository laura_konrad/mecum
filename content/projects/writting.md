---
name: writing stories
date: 2016-01-01
image: "https://images.unsplash.com/reserve/LJIZlzHgQ7WPSh5KVTCB_Typewriter.jpg?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=641&q=80"
link: "https://storyloom.de"
---
It's more a hobby than a real project. I started writing at the age of 16 and am still addicted to my mode. For a long time, they were only stored on my PC. Because of positive feedback, I wanted to share them. Currently, not all of them are published because they need a small update.

After a while, I also started a kind of travel diary and wanted to archive recipes. I thought this can also be done by sharing and publish, but still have to digitize a lot of them. 