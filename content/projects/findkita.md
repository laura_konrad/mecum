---
name: Find-Kita
date: 2016-09-11
image: "https://images.unsplash.com/photo-1558877385-6fc9b25e3adf?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=919&q=80"
---

It is impossible how complicated it could be to find a kindergarten place. So I wanted to make this easier and started to develop a platform. At some point, I noticed that the government of Berlin itself also implemented something similar. I tried to connect to this and talked to those responsible and get a talk. Because of the restriction they made and the requirement that the kindergartens in berlin have to use their system, I stopped investing time in it, and finding and connect to a kindergarten stays complicate.

Tools and languages: Nodejs, Express, Mustach, Apache Cassandra, Redis, HereMaps

The code stays private.
