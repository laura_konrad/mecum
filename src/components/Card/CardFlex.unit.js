import { shallowMount } from '@vue/test-utils';
import CardFlex from './CardFlex.vue';

const someComponent = {
    name: 'test',
    template: '<div><p>Text</p></div>'
}

const someOther = {
    name: 'blub',
    template: '<section>ttt</section>'
}

describe('CardFlex', () => {
    it('implement without content', () => {
        const wrapper = shallowMount(CardFlex)
        expect(wrapper.html()).toBe('')
    })

    it('implement empty list', () => {
        const wrapper = shallowMount(CardFlex, {
            propsData: {
                allowEmptyList: true
            }
        })
        expect(wrapper.html()).toBe('<ul></ul>')
    })

    it('implement empty list with class', () => {
        const wrapper = shallowMount(CardFlex, {
            propsData: {
                cssClass: 'first second',
                allowEmptyList: true
            }
        })
        expect(wrapper.find('ul.first.second').isVisible()).toBe(true);
    })

    it('list with elements', () => {
        const wrapper = shallowMount(CardFlex, {
            propsData: {
                cssClass: 'flex-list',
            },
            slots:{
                default: [someComponent, someOther]
            }
        })
        const ul = wrapper.find('ul.flex-list');
        expect(ul.isVisible()).toBe(true);
        const lis = ul.findAll('li');
        expect(lis.length).toBe(2);
        expect(lis.at(0).text()).toBe('Text')
        expect(lis.at(1).text()).toBe('ttt')

    })
})