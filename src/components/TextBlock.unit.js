import { shallowMount } from '@vue/test-utils';
import TextBlock from './TextBlock.vue';

const createTextBlock = (props) => {
    return shallowMount(TextBlock, {
        propsData: {
            ...props
        }
    })
}

describe('TextBlock', () => {
    it('mount component', () => {
        const content = "some content"
        const wrapper = createTextBlock({
            content
        });

        expect(wrapper.text()).toMatch(content);
    })

    it('render html', () => {
        const html = `
            <h3>Text</h3>
            <p>Some text</p>
            <p class="ape">some other texts</p>
        `

        const wrapper = createTextBlock({
            content: html
        })

        const h3 = wrapper.find('h3') 
        expect(h3.exists()).toBe(true);
        expect(h3.text()).toMatch('Text');

        const p = wrapper.findAll('p');
        expect(p.length).toBe(2);

        const ape = wrapper.find('.ape');
        expect(ape.text()).toMatch('some other texts');
    })
})