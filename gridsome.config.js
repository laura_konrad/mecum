// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here requires a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'Wolfgang Rathgeb',
  siteDescription: 'Software Engineer',
  outputDir: 'public',
  pathPregix: '/',

  plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/text/*.md',
        typeName: 'Text',
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/projects/*.md',
        typeName: 'Project',
      },
    }
  ],

  transformers: {
    //Add markdown support to all file-system sources
    remark: {
      anchorClassName: 'icon icon-link test',
      squesqueezeParagraphs: true,
      plugins: [
        '@gridsome/remark-prismjs',
        'remark-breaks'
      ]
    }
  }
}
